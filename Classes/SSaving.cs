﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class SSaving
    {
        public static void SaveLine(string line, string patch)
        {
            string directory = patch + ".txt";

            if (!File.Exists(directory))
                using (File.Create(directory)) { }

            using (StreamWriter sw = new StreamWriter(directory, true, Encoding.Default))
            {
                sw.WriteLine($"{line}");
            }
        }

    }
}
