﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class DB_Controller
    {
        public static List<string> list = new List<string>();
        public static void ReadSave(string patch)
        {
            try
            {
                using (StreamReader name = new StreamReader(patch, Encoding.Default))
                {
                    string line;

                    while ((line = name.ReadLine()) != null)
                    {
                        list.Add(line);
                    }
                }
            }
            catch 
            {
                return;
            }
        }
    }
}
